name := "order-checkout-exercise"

version := "0.1"

scalaVersion := "2.13.6"

  val akkaVersion = "2.6.8"
  val akkaHttpVersion = "10.2.4"
  val scalaTestVersion        = "3.2.9"
  val akkaTestkitVersion      = "2.6.8"
  val akkaHttpTestkitVersion  = "10.2.4"
  val circeVersion            = "0.12.3"
  val akkaHttpCirceVersion    = "1.36.0"

libraryDependencies ++= Seq(
  "com.typesafe.akka" %% "akka-actor" % akkaVersion,
  "com.typesafe.akka" %% "akka-stream" % akkaVersion,
  "com.typesafe.akka" %% "akka-http" % akkaHttpVersion,
)

libraryDependencies ++= Seq(
  "io.circe" %% "circe-core" % circeVersion,
  "io.circe" %% "circe-generic" % circeVersion,
  "io.circe" %% "circe-parser" % circeVersion,
)

libraryDependencies += "org.scalatest" %% "scalatest" % scalaTestVersion % Test

libraryDependencies +=
  "de.heikoseeberger" %% "akka-http-circe" % akkaHttpCirceVersion

libraryDependencies +=
  "com.typesafe.akka" %% "akka-testkit" % akkaTestkitVersion % Test

libraryDependencies +=
  "com.typesafe.akka" %% "akka-http-testkit" % akkaHttpTestkitVersion % Test
