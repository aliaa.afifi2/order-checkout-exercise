package Lib

import sbt.Keys.libraryDependencies
import sbt._

object Lib {

  object Version {
    val circe            = "0.11.1"
    val http4s           = "0.20.0"
//    val akkaActor        = "2.5.21"
    val akkaStream       = "2.5.21"
    val akkaSlf4j        = "2.5.21"
//    val akkaHttp         = "10.1.8"
    val akkaTestkit      = "2.5.21"
    val akkaHttpCirce    = "1.25.2"
    val scalaTest        = "3.0.7"
    val akkaHttpTestkit  = "10.1.7"


    val Akka = "2.6.8"
    val AkkaHttp = "10.2.4"
  }

  libraryDependencies ++= Seq(
    "com.typesafe.akka" %% "akka-actor-typed" % Version.Akka,
    "com.typesafe.akka" %% "akka-stream" % Version.Akka,
    "com.typesafe.akka" %% "akka-http" % Version.AkkaHttp
  )


}
