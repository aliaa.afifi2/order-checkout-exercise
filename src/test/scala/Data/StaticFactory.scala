package Data

import model._

import java.util.UUID

object StaticFactory {

  val orderCheckoutResponse: OrderCheckoutResponse = OrderCheckoutResponse(
    UUID.randomUUID(),
    "success",
    "submitted",
    200.0,
    Vector.empty[OrderCheckoutResponseItem]
  )

  val orderCheckoutRequest: OrderCheckoutRequest = OrderCheckoutRequest(
    CustomerData(
      Some("Alice"),
      "12345678",
      "a@gmail.com",
      "123456",
    ),
    PaymentData(
      "1234567812345678",
      "25",
      "02",
      "123"
    )
  )

}
