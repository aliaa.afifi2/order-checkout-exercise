package service

import model.{FawryPayResponse, OrderCheckoutResponse, OrderItem}
import org.scalatest.flatspec.AsyncFlatSpec
import Data.StaticFactory

import java.util.UUID
import scala.concurrent.Future

class OrderServiceSpec extends AsyncFlatSpec {

  val orderService: DefaultOrderService = new DefaultOrderService {
    override val paymentService: DefaultPaymentService = new DefaultPaymentService {
      override def sendFawryPayRequest(body: String): Future[String] =
        Future.successful("\"statusCode\": 200," +
          "\"statusDescription\": \"Operation done successfully\"")

      override def parseFawryPayJsonToObject(payFawryResponse: String): Future[FawryPayResponse] =
        Future.successful(FawryPayResponse(200, "Operation done successfully"))
    }
  }

  behavior of "validating order items uuids"
  it should "return that the order items uuids exist" in {

    val items = Vector(
      OrderItem(UUID.fromString("508682e3-685e-421c-9bef-ab1f2ec9f463"), 1)
    )

    orderService.validateItemsId(items) match {
      case Left(_) => fail
      case Right(value) => assert(value)
    }
  }

  it should "return that the order items uuids don't exist" in {

    val items = Vector(
      OrderItem(UUID.fromString("508682e3-685e-421c-9bef-ab1f2ec9f433"), 2),
      OrderItem(UUID.fromString("508682e3-685e-421c-9bef-ab1f2ec9f463"), 2)
    )

    orderService.validateItemsId(items) match {
      case Left(message) => assert(message.contains("508682e3-685e-421c-9bef-ab1f2ec9f433"))
      case Right(_) => fail
    }

  }

  behavior of "validating order items availability"
  it should "return that all the order items are available" in {

    val items = Vector(
      OrderItem(UUID.fromString("508682e3-685e-421c-9bef-ab1f2ec9f463"), 1),
      OrderItem(UUID.fromString("a42b6e33-ed90-479f-92e3-36caca75b65d"), 5)
    )

    orderService.validateItemsAvailability(items) match {
      case Left(_) => fail
      case Right(value) => assert(value)
    }
  }

  it should "return that the order contains unavailable items" in {

    val items = Vector(
      OrderItem(UUID.fromString("508682e3-685e-421c-9bef-ab1f2ec9f463"), 1),
      OrderItem(UUID.fromString("a42b6e33-ed90-479f-92e3-36caca75b65d"), 3),
      OrderItem(UUID.fromString("8af6bc7b-436d-416e-a0f0-323e083f404f"), 2)
    )

    orderService.validateItemsAvailability(items) match {
      case Left(message) => assert(message.contains("8af6bc7b-436d-416e-a0f0-323e083f404f"))
      case Right(_) => fail
    }

  }

  behavior of "validating order total amount"
  it should "return that the order total amount exceeds the minimum value" in {

    val items = Vector(
      OrderItem(UUID.fromString("508682e3-685e-421c-9bef-ab1f2ec9f463"), 1),
      OrderItem(UUID.fromString("a42b6e33-ed90-479f-92e3-36caca75b65d"), 10)
    )

    orderService.validateMinimumOrderAmount(items) match {
      case Left(_) => fail
      case Right(value) => assert(value)
    }
  }

  it should "return that the order total value doesn't exceed minimum value" in {

    val items = Vector(
      OrderItem(UUID.fromString("508682e3-685e-421c-9bef-ab1f2ec9f463"), 1)
    )

    orderService.validateMinimumOrderAmount(items) match {
      case Left(_) => succeed
      case Right(_) => fail
    }

  }

  behavior of "validating user fraud"
  it should "return that the user is fraud" in {

    val items = Vector(
      OrderItem(UUID.fromString("508682e3-685e-421c-9bef-ab1f2ec9f463"), 10),
      OrderItem(UUID.fromString("a42b6e33-ed90-479f-92e3-36caca75b65d"), 5),
      OrderItem(UUID.fromString("cd76e839-fc1d-4ec8-8440-c9e24742b2f5"), 10)
    )

    orderService.checkUserFraud(items) match {
      case Left(_) => succeed
      case Right(_) => fail
    }
  }

  it should "return that the user is not fraud" in {

    val items = Vector(
      OrderItem(UUID.fromString("508682e3-685e-421c-9bef-ab1f2ec9f463"), 10)
    )

    orderService.checkUserFraud(items) match {
      case Left(_) => fail
      case Right(value) => assert(value)
    }

  }

  behavior of "validating the order"
  it should "return that the order is valid" in {

    val orderUUID = UUID.fromString("2ce5e975-b0b4-4517-95a7-0bbbb8a9d07f")

    orderService.validateOrder(orderUUID) match {
      case Left(_) => fail
      case Right(_) => succeed
    }
  }

  it should "return that the order is not valid (item is not available)" in {

    val orderUUID = UUID.fromString("a6c0c015-cadf-4e7e-a3c0-b60a98c3991e")

    orderService.validateOrder(orderUUID) match {
      case Left(message) => assert(message.contains("f3c19180-679b-4801-b57b-02e04b1e1237"))
      case Right(_) => fail
    }

  }

  it should "return that the order is not valid (total amount less than the minimum value)" in {

    val orderUUID = UUID.fromString("7fb73b87-e6b4-4a64-abfe-f19db15647a3")

    orderService.validateOrder(orderUUID) match {
      case Left(_) => succeed
      case Right(_) => fail
    }

  }

  it should "return that the order is not valid (user is fraud)" in {

    val orderUUID = UUID.fromString("c3214ade-7a21-4544-a90f-fc84dc879165")

    orderService.validateOrder(orderUUID) match {
      case Left(_) => succeed
      case Right(_) => fail
    }
  }

  behavior of "checkout order"
  it should "checkout order successfully" in {

    val orderUUID = UUID.fromString("2ce5e975-b0b4-4517-95a7-0bbbb8a9d07f")
    val checkoutResponse: Future[OrderCheckoutResponse] = orderService.checkoutOrder(orderUUID, StaticFactory.orderCheckoutRequest)

    checkoutResponse map {
      case response: OrderCheckoutResponse =>
        assert(response.uuid == orderUUID)
        assert(response.paymentStatus == "Operation done successfully")
        assert(response.orderStatus == "Submitted")
        assert(response.totalAmount == 746.0)
      case _ => fail
    }
  }
}