package route

import Data.StaticFactory
import akka.http.scaladsl.testkit.ScalatestRouteTest
import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport
import org.scalatest.flatspec.AsyncFlatSpec
import org.scalatest.matchers.should.Matchers
import model.{OrderCheckoutRequest, OrderCheckoutResponse}
import service.DefaultOrderService

import java.util.UUID
import scala.concurrent.Future
import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.Route

class OrderRouteSpec extends AsyncFlatSpec with Matchers with FailFastCirceSupport with ScalatestRouteTest {

  def orderRoute: OrderRoute = new OrderRoute {
    override val orderService: DefaultOrderService = new DefaultOrderService {
      override def validateOrder(uuid: UUID): Either[String, Boolean] = Right(true)

      override def checkoutOrder(orderUUID: UUID, orderCheckoutRequest: OrderCheckoutRequest): Future[OrderCheckoutResponse] =
        Future.successful(StaticFactory.orderCheckoutResponse)
    }
  }

  def route: Route = orderRoute.route

    it should "validate order" in {
      val url = s"http://localhost:8000/order/${UUID.randomUUID}/validate"

      Get(url) ~> route ~> check {
        assert(response.status == StatusCodes.OK)
      }
    }

    it should "checkout order" in {
      val url = s"http://localhost:8000/order/${UUID.randomUUID}/checkout"

      Post(url, StaticFactory.orderCheckoutRequest) ~> route ~> check {
        assert(response.status == StatusCodes.OK)
        assert(responseAs[OrderCheckoutResponse] == StaticFactory.orderCheckoutResponse)
      }
    }

}


