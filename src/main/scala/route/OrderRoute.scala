package route

import akka.actor.ActorSystem
import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.Directives.{JavaUUID, complete, entity, get, pathEndOrSingleSlash, pathPrefix, post}
import akka.http.scaladsl.server.{ExceptionHandler, Route}
import model.OrderCheckoutRequest
import service.DefaultOrderService
import io.circe.generic.auto._
import akka.http.scaladsl.server.Directives._
import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport._
import io.circe.Json
import io.circe.syntax._

import scala.concurrent.ExecutionContext

class OrderRoute {

  implicit val actorSystem: ActorSystem = ActorSystem()
  implicit val executionContext: ExecutionContext = actorSystem.dispatcher

  val orderService = new DefaultOrderService

  implicit def exceptionHandler: ExceptionHandler = ExceptionHandler {
    case ex: Throwable => complete((StatusCodes.BadRequest, Json.obj("error" -> ex.getMessage.asJson)))
  }

  def route: Route = {
    handleExceptions(exceptionHandler) {
      pathPrefix("order") {
        pathPrefix(JavaUUID) { uuid =>
          pathPrefix("validate") {
            pathEndOrSingleSlash {
              get {
                orderService.validateOrder(uuid) match {
                  case Left(value) => new Throwable(value)
                  case Right(_) => complete("Valid Order")
                }
              }
            }
          } ~
            pathPrefix("checkout") {
              pathEndOrSingleSlash {
                post {
                  entity(as[OrderCheckoutRequest]) { order =>
                    println(s"order: $order")
                    onComplete(orderService.checkoutOrder(uuid, order)) { response =>
                      complete(response)
                    }
                  }
                }
              }
            }
        }
      }
    }
  }
}