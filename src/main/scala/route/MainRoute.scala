package route

import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route

class MainRoute {

  val orderRoute = new OrderRoute

  def route: Route =
    path("health") {
      get {
        complete("Service is up and running!")
      }
    } ~
      orderRoute.route

}
