package model

import io.circe.{Decoder, Encoder}
import io.circe.generic.semiauto.{deriveDecoder, deriveEncoder}

case class FawryPayChargeItems(itemId: String, description: String, price: Double, quantity: String)

case class FawryPayRequest(merchantCode: String, customerName: Option[String], customerMobile: String,
  customerEmail: String, customerProfileId: String, cardNumber: String, cardExpiryYear: String,
  cardExpiryMonth: String, cvv: String, merchantRefNum: String, amount: Double, currencyCode: String,
  language : String, chargeItems: Vector[FawryPayChargeItems], signature: String,
  paymentMethod: String, description: String)

case class FawryPayResponse(statusCode: Int, statusDescription: String)

object FawryPayResponse {
  implicit val decoder: Decoder[FawryPayResponse] = deriveDecoder[FawryPayResponse]
  implicit val encoder: Encoder[FawryPayResponse] = deriveEncoder[FawryPayResponse]
}