package model

import io.circe.{Decoder, Encoder}
import io.circe.generic.semiauto.{deriveDecoder, deriveEncoder}

import java.util.UUID

case class OrderCheckoutResponseItem(uuid: UUID, name: String, price: Double, count: Int)

object OrderCheckoutResponseItem {
  implicit val decoder: Decoder[OrderCheckoutResponseItem] = deriveDecoder[OrderCheckoutResponseItem]
  implicit val encoder: Encoder[OrderCheckoutResponseItem] = deriveEncoder[OrderCheckoutResponseItem]
}
