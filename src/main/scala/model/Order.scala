package model

import java.util.UUID

case class Order(uuid: UUID, items: Vector[OrderItem])
