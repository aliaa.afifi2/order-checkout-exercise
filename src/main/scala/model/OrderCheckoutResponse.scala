package model

import io.circe.{Decoder, Encoder}
import io.circe.generic.semiauto.{deriveDecoder, deriveEncoder}

import java.util.UUID

case class OrderCheckoutResponse(uuid: UUID,
                                paymentStatus: String,
                                orderStatus: String,
                                totalAmount: Double,
                                items: Vector[OrderCheckoutResponseItem])

object OrderCheckoutResponse {
  implicit val decoder: Decoder[OrderCheckoutResponse] = deriveDecoder[OrderCheckoutResponse]
  implicit val encoder: Encoder[OrderCheckoutResponse] = deriveEncoder[OrderCheckoutResponse]
}
