package model

import java.util.UUID

case class Item(uuid: UUID, name: String, availability: Boolean, price: Double)

