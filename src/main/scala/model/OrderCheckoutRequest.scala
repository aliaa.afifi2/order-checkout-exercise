package model

import io.circe.{Decoder, Encoder}
import io.circe.generic.semiauto.{deriveDecoder, deriveEncoder}

case class OrderCheckoutRequest(customerData: CustomerData, paymentData: PaymentData)

object OrderCheckoutRequest {
  implicit val decoder: Decoder[OrderCheckoutRequest] = deriveDecoder[OrderCheckoutRequest]
  implicit val encoder: Encoder[OrderCheckoutRequest] = deriveEncoder[OrderCheckoutRequest]
}