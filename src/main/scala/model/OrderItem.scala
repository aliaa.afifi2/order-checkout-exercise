package model

import java.util.UUID

case class OrderItem(itemUUID: UUID, count: Int)
