package model

import io.circe.{Decoder, Encoder}
import io.circe.generic.semiauto.{deriveDecoder, deriveEncoder}

case class CustomerData(name: Option[String], phoneNumber: String, email: String, profileId: String)

object CustomerData {
  implicit val decoder: Decoder[CustomerData] = deriveDecoder[CustomerData]
  implicit val encoder: Encoder[CustomerData] = deriveEncoder[CustomerData]
}