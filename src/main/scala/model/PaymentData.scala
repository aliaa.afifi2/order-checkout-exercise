package model

import io.circe.{Decoder, Encoder}
import io.circe.generic.semiauto.{deriveDecoder, deriveEncoder}

case class PaymentData(cardNumber: String, cardExpiryYear: String, cardExpiryMonth: String, cvv: String)

object PaymentData {
  implicit val decoder: Decoder[PaymentData] = deriveDecoder[PaymentData]
  implicit val encoder: Encoder[PaymentData] = deriveEncoder[PaymentData]
}