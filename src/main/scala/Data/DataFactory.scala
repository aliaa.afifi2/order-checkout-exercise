package Data

import model.{Item, Order, OrderItem}

import java.util.UUID

object DataFactory {
  val ITEMS = Vector(
    Item(UUID.fromString("508682e3-685e-421c-9bef-ab1f2ec9f463"), "Beef sandwich", availability = true, 65),
    Item(UUID.fromString("a42b6e33-ed90-479f-92e3-36caca75b65d"), "Pepsi", availability = true, 4.5),
    Item(UUID.fromString("05edca78-15af-4551-87f5-ffd27a3c4a96"), "Chicken sandwich", availability = false, 55),
    Item(UUID.fromString("f3c19180-679b-4801-b57b-02e04b1e1237"), "Ketchup", availability = false, 1.5),
    Item(UUID.fromString("cd76e839-fc1d-4ec8-8440-c9e24742b2f5"), "Family meal", availability = true, 125),
    Item(UUID.fromString("19b5ebc6-2be3-4f07-995b-799d7db8b428"), "French fries", availability = true, 24.5),
    Item(UUID.fromString("8af6bc7b-436d-416e-a0f0-323e083f404f"), "Dinner box", availability = false, 70)
  )

  val ORDERS = Vector(
    Order(
      UUID.fromString("2ce5e975-b0b4-4517-95a7-0bbbb8a9d07f"),
      Vector(
        OrderItem(UUID.fromString("508682e3-685e-421c-9bef-ab1f2ec9f463"), 10),
        OrderItem(UUID.fromString("a42b6e33-ed90-479f-92e3-36caca75b65d"), 5),
        OrderItem(UUID.fromString("19b5ebc6-2be3-4f07-995b-799d7db8b428"), 3),
      )
    ),
    Order(
      UUID.fromString("a6c0c015-cadf-4e7e-a3c0-b60a98c3991e"),
      Vector(
        OrderItem(UUID.fromString("f3c19180-679b-4801-b57b-02e04b1e1237"), 1),
      )
    ),
    Order(
      UUID.fromString("7fb73b87-e6b4-4a64-abfe-f19db15647a3"),
      Vector(
        OrderItem(UUID.fromString("508682e3-685e-421c-9bef-ab1f2ec9f463"), 1),
      )
    ),
    Order(
      UUID.fromString("c3214ade-7a21-4544-a90f-fc84dc879165"),
      Vector(
        OrderItem(UUID.fromString("cd76e839-fc1d-4ec8-8440-c9e24742b2f5"), 10),
        OrderItem(UUID.fromString("508682e3-685e-421c-9bef-ab1f2ec9f463"), 5),
      )
    ),
  )
}
