import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import route.MainRoute

import scala.concurrent.{ExecutionContext, Future}
import scala.util.{Failure, Success}

object Service extends App {

  implicit val actorSystem: ActorSystem = ActorSystem()
  implicit val executionContext: ExecutionContext = actorSystem.dispatcher

  val mainRoute = new MainRoute

  val host = "0.0.0.0"
  val port = 8000

  val server: Future[Http.ServerBinding] = Http().newServerAt(host, port).bindFlow(mainRoute.route)

  server.onComplete {
    case Failure(exception) => println(s"Can't start the server: $exception")
    case Success(_) => println(s"Server is up and running on port $port")
  }

}
