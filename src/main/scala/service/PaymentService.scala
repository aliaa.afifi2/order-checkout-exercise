package service

import model.{FawryPayResponse, Order, OrderCheckoutRequest}

import scala.concurrent.Future

trait PaymentService {

  def constructFawryPayRequest(orderCheckoutRequest: OrderCheckoutRequest, order: Order): Future[String]

  def sendFawryPayRequest(body: String): Future[String]

  def parseFawryPayJsonToObject(payFawryResponse: String): Future[FawryPayResponse]
}
