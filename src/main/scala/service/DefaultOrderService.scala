package service

import Data.DataFactory
import akka.actor.ActorSystem
import model.{FawryPayResponse, Order, OrderCheckoutRequest, OrderCheckoutResponse, OrderCheckoutResponseItem, OrderItem}

import java.util.UUID
import scala.concurrent.{ExecutionContext, Future}

class DefaultOrderService extends OrderService {

  implicit val actorSystem: ActorSystem = ActorSystem()
  implicit val executionContext: ExecutionContext = actorSystem.dispatcher

  val paymentService = new DefaultPaymentService

  val MIN_ORDER_AMOUNT = 100

  def validateItemsId(items: Vector[OrderItem]): Either[String, Boolean] = {
    val notValidIdsList = items.map(_.itemUUID).diff(DataFactory.ITEMS.map(_.uuid))
    notValidIdsList.length match {
      case 0 => Right(true)
      case _ => Left(s"Invalid uuid(s): ${notValidIdsList.mkString(", ")}")
    }
  }

  def validateItemsAvailability(items: Vector[OrderItem]): Either[String, Boolean] = {
    val availableItems = items.filter(item =>
      DataFactory.ITEMS.find(i => i.uuid == item.itemUUID && i.availability) match {
        case Some(_) => true
        case None => false
      }
    )

    val notAvailableItems: Vector[OrderItem] = items.diff(availableItems)
    if(notAvailableItems.isEmpty)
      Right(true)
    else
      Left(s"The following item(s) is/are not available: ${notAvailableItems.map(_.itemUUID).mkString(", ")}")

  }

  def validateMinimumOrderAmount(items: Vector[OrderItem]): Either[String, Boolean] = {
    val totalAmount = items.foldLeft(0.0)((acc: Double, item: OrderItem) => acc + (
        item.count * DataFactory.ITEMS.find(i => i.uuid == item.itemUUID && i.availability).map(_.price).getOrElse(0.0)
      )
    )

    if (totalAmount < MIN_ORDER_AMOUNT)
      Left(s"Order total amount can't be less than $MIN_ORDER_AMOUNT")
    else
      Right(true)
  }

  def checkUserFraud(items: Vector[OrderItem]): Either[String, Boolean] = {
    val totalAmount = items.foldLeft(0.0)((acc: Double, item: OrderItem) => acc + (
      item.count * DataFactory.ITEMS.find(i => i.uuid == item.itemUUID && i.availability).map(_.price).getOrElse(0.0)
      )
    )

    if (totalAmount > 1500)
      Left(s"fraud warning")
    else
      Right(true)
  }

  def validateOrder(uuid: UUID): Either[String, Boolean] = {
    DataFactory.ORDERS.find(i => i.uuid == uuid) match {
      case Some(value) =>
        for {
          areValidIds <- validateItemsId(value.items)
          areAvailableItems <- validateItemsAvailability(value.items)
          isValidTotalAmount <- validateMinimumOrderAmount(value.items)
          isFraud <- checkUserFraud(value.items)
        } yield areValidIds && areAvailableItems && isValidTotalAmount && isFraud
      case None => Left(s"Can't find order with uuid: $uuid")
    }
  }

  def constructSummaryResponse(orderUUID: UUID, order: Order, payingResponse: FawryPayResponse): OrderCheckoutResponse = {
    OrderCheckoutResponse(
      orderUUID,
      payingResponse.statusDescription,
      "Submitted",
      order.items.foldLeft(0.0)((acc: Double, item: OrderItem) => acc + (
        item.count * DataFactory.ITEMS.find(i => i.uuid == item.itemUUID && i.availability).map(_.price).getOrElse(0.0)
        )
      ),
      order.items.map(item =>
        OrderCheckoutResponseItem(
          item.itemUUID,
          DataFactory.ITEMS.find(_.uuid == item.itemUUID).map(_.name).getOrElse(""),
          DataFactory.ITEMS.find(_.uuid == item.itemUUID).map(_.price).getOrElse(0.0),
          item.count
        )
      )

    )
  }

  def checkoutOrder(orderUUID: UUID, orderCheckoutRequest: OrderCheckoutRequest): Future[OrderCheckoutResponse] = {

    DataFactory.ORDERS.find(i => i.uuid == orderUUID) match {
      case Some(orderValue) =>
        validateOrder(orderUUID) match {
          case Left(value) => Future.failed(new Throwable(value))
          case Right(_) =>
            for {
              fawryPayRequest <- paymentService.constructFawryPayRequest(orderCheckoutRequest, orderValue)
              payFawryResponse <- paymentService.sendFawryPayRequest(fawryPayRequest)
              parsedResponse <- paymentService.parseFawryPayJsonToObject(payFawryResponse)
              checkoutResponse <- Future { constructSummaryResponse(orderUUID, orderValue, parsedResponse) }
            } yield checkoutResponse
        }
      case None => Future.failed(new Throwable("order Not found"))
    }
  }

}
