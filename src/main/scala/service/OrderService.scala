package service

import model.{OrderCheckoutRequest, OrderCheckoutResponse}

import java.util.UUID
import scala.concurrent.Future

trait OrderService {
  def validateOrder(uuid: UUID): Either[String, Boolean]

  def checkoutOrder(orderUUID: UUID, orderCheckoutRequest: OrderCheckoutRequest): Future[OrderCheckoutResponse]
}
