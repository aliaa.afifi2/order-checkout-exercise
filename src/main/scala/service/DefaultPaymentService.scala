package service

import Data.DataFactory
import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.{ContentType, HttpEntity, HttpMethods, HttpRequest, MediaTypes}
import akka.http.scaladsl.unmarshalling.Unmarshal
import io.circe.{Json, jawn}
import io.circe.syntax._
import model.{FawryPayResponse, Order, OrderCheckoutRequest, OrderItem}

import scala.concurrent.{ExecutionContext, Future}

class DefaultPaymentService {

  implicit val actorSystem: ActorSystem = ActorSystem()
  implicit val executionContext: ExecutionContext = actorSystem.dispatcher

  def constructFawryPayRequest(orderCheckoutRequest: OrderCheckoutRequest, order: Order): Future[String] = {

    val MERCHANT_CODE = "1tSa6uxz2nTwlaAmt38enA=="
    val MERCHANT_REF_NUM = "2312465464"

    val totalAmount: Double = order.items.foldLeft(0.0)((acc: Double, item: OrderItem) => acc + (
      item.count * DataFactory.ITEMS.find(i => i.uuid == item.itemUUID && i.availability).map(_.price).getOrElse(0.0)
      )
    )

    val reqBody: String = Json
      .obj(
        "merchantCode" -> MERCHANT_CODE.asJson,
        "customerName" -> orderCheckoutRequest.customerData.name.asJson,
        "customerMobile" -> orderCheckoutRequest.customerData.phoneNumber.asJson,
        "customerEmail" -> orderCheckoutRequest.customerData.email.asJson,
        "customerProfileId" -> orderCheckoutRequest.customerData.profileId.asJson,
        "cardNumber" -> orderCheckoutRequest.paymentData.cardNumber.asJson,
        "cardExpiryYear" -> orderCheckoutRequest.paymentData.cardExpiryYear.asJson,
        "cardExpiryMonth" -> orderCheckoutRequest.paymentData.cardExpiryMonth.asJson,
        "cvv" -> orderCheckoutRequest.paymentData.cvv.asJson,
        "merchantRefNum" -> MERCHANT_REF_NUM.asJson,
        "amount" -> totalAmount.asJson,
        "currencyCode" -> "EGP".asJson,
        "language"  -> "ar-eg".asJson,
        "chargeItems" ->
          order.items.map(item =>
            Json.obj(
              "itemId" -> item.itemUUID.toString.asJson,
              "description" -> DataFactory.ITEMS.find(i => i.uuid == item.itemUUID).map(_.name).getOrElse("").asJson,
              "price" -> DataFactory.ITEMS.find(i => i.uuid == item.itemUUID).map(_.price).getOrElse(0.0).asJson,
              "quantity" -> item.count.toString.asJson
            )
          ).asJson,
        "signature" -> "2ca4c078ab0d4c50ba90e31b3b0339d4d4ae5b32f97092dd9e9c07888c7eef33".asJson,
        "paymentMethod" -> "CARD".asJson,
        "description" -> orderCheckoutRequest.customerData.name.getOrElse(orderCheckoutRequest.customerData.phoneNumber).asJson
      ).noSpaces

    Future.successful(reqBody)
  }

  def sendFawryPayRequest(body: String): Future[String] = {
    val httpRequest = HttpRequest(
      method = HttpMethods.POST,
      uri = "https://atfawry.fawrystaging.com/ECommerceWeb/Fawry/payments/charge",
      entity = HttpEntity(
        ContentType(MediaTypes.`application/json`),
        body
      )
    )

    Http()
      .singleRequest(httpRequest)
      .flatMap(response => {
        Unmarshal(response.entity)
          .to[String]
      })
  }

  def parseFawryPayJsonToObject(payFawryResponse: String): Future[FawryPayResponse] = {
    jawn.decode[FawryPayResponse](payFawryResponse) match {
      case Right(value) => Future.successful(value)
      case Left(ex) => Future.failed(new Throwable(ex))
    }
  }

}
